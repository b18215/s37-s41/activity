//----------SECTION: Dependencies --------------------------------------
const express = require('express');
const router = express.Router();

//--------------- Router method - allows access to HTTP methods-----------

const courseControllers = require('../controllers/courseControllers');

//------------------------ Added from Auth ----------------------------

const auth = require('../auth');

//object destructuring from auth module
const { verify, verifyAdmin } = auth;


//------------create course route-----------------------------------------
router.post('/', verify, verifyAdmin, courseControllers.registerCourse);

//--------------get all course route-------------------------------------
router.get('/', courseControllers.getAllCourses);


//-----------------get single user route-----------------------------------
router.get('/getSingleCourse/:id', courseControllers.getSingleCourse);



//--------------------updateCourse--------------------------------
router.put('/:id', verify, verifyAdmin, courseControllers.updateCourse);



//--------------------/archive-course/:id--------------------------------
router.put('/archive/:id', verify, verifyAdmin, courseControllers.archiveCourse);


//--------------------/archive=course/:id--------------------------------
router.put('/activate/:id', verify, verifyAdmin, courseControllers.activateCourse);

//--------------------getActiveCourses-----------------------------------
router.get('/getActiveCourses', courseControllers.getActiveCourses);


//--------------------Get Enrollments--------------------------------

router.post('/findCoursesByName', courseControllers.findCoursesByName);
/*


//update single user route
router.put('/updateUser/:id', userControllers.updateUserUsernameController);



*/
module.exports = router;