//----------SECTION: Dependencies --------------------------------------
const express = require('express');
const router = express.Router();

//--------------- Router method - allows access to HTTP methods-----------

const userControllers = require('../controllers/userControllers');

const auth = require('../auth');

//object destructuring from auth module


const { verify, verifyAdmin } = auth;


//------------create user route-----------------------------------------
router.post('/', userControllers.registerUser);

//-----------get all users route----------------------------------------
router.get('/', userControllers.getAllUsers);

//------------------------Login User------------------------------------
router.post('/login', userControllers.loginUser);


//------------------getUserDetails--------------------------------------
router.get('/getUserDetails', verify, userControllers.getUserDetails);

//------------------------checkEmailExists------------------------------
router.get('/checkEmailExists', userControllers.checkEmailExists);

//--------------------updateUserDetails--------------------------------
router.put('/updateUserDetails', verify, userControllers.updateUserDetails);

//-----------------updateAdmin---------------------------------------------
//--------------------updateUserDetails--------------------------------
router.put('/updateAdmin/:id', verify, verifyAdmin, userControllers.updateAdmin);

//--------------------Enroll--------------------------------

router.post('/enroll', verify, userControllers.enroll);

//--------------------Get Enrollments--------------------------------

router.get('/getEnrollments', verify, userControllers.getEnrollments);





module.exports = router;