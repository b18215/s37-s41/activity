
// -------------------------SCHEMA--------------------------------

const mongoose = require('mongoose');

// Schema - a blueprint for our data/document 
const userSchema = new mongoose.Schema({


	firstName: {
		type: String,
		required: [true, "Firstname is required"]
	},
	lastName: {
		type: String,
		required: [true, "Lastname is required"]
	},
	email: {
		type: String,
		required: [true, "Email address is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	mobileNumber: {
		type: String,
		required: [true, "Mobile number is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	enrollments: [
		{
			courseId: {
				type: String,
				required: [true, "course ID is required"]
				},
			dateEnrolled: {
				type: Date,
				default: new Date()
				},
			status: {
				type: String,
				default: "Enrolled"
				}

		}
	]

})

// ---------------------MONGOOSE MODEL----------------------------
/*
	mongoose.model(<nameOfCollectionInAtlas>, <schedmaToFollow>)

*/

module.exports = mongoose.model("User", userSchema);