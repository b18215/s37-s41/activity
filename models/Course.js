
// -------------------------SCHEMA--------------------------------

const mongoose = require('mongoose');

// Schema - a blueprint for our data/document 
const courseSchema = new mongoose.Schema({

	name: {
		type: String,
		required: [true, "Course name is required"]
	},
	description: {
		type: String,
		required: [true, "Course description is required"]
	},
	price: {
		type: Number,
		required: [true, "Course price is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	enrollees: [
		{
			userId: {
				type: String,
				required: [true, "userID is required"]
				},
			dateEnrolled: {
				type: Date,
				default: new Date()
				},
			status: {
				type: String,
				default: "Enrolled"
				}

		}
	]

})

// ---------------------MONGOOSE MODEL----------------------------
/*
	mongoose.model(<nameOfCollectionInAtlas>, <schedmaToFollow>)

*/

module.exports = mongoose.model("Course", courseSchema);