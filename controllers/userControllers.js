// import the user model in our controllers. So that our controllers or controller function may have access to our user model.
const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require('bcryptjs');

const auth = require("../auth");

//----------------------Create user---------------------------

module.exports.registerUser = (req, res) => {
	console.log(req.body);


	/*
		bcrypt.hashSync(<stringToBeHashed>,<saltRounds>)

		salt rounds - number of times the characters in the hash randomized
	*/

	const hashedPW = bcrypt.hashSync(req.body.password, 10);

	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		mobileNumber: req.body.mobileNumber,
		password: hashedPW
	});

	newUser.save()
	.then(user => res.send(user))
	.catch(error => res.send(error));
};

//--------------------RETRIEVAL OF ALL USERS-----------------
module.exports.getAllUsers = (req, res) => {
	User.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));
}

//---------SECTION: Login User------------------------------
module.exports.loginUser = (req, res) => {
	console.log(req.body)

/*
	1. find the user by the email
	2. If we found user, we will check the password
	3. if we don't find the user, then we will send a message to the Client.
	4. If upon checking the found user's password is the same as our input password, we will generate the 'token / key' to access our app. If not, we will turn them away by sending a message to the Client.
*/


	User.findOne({email: req.body.email})
	.then(foundUser => {
		if(foundUser === null){
			return res.send("No user found in the database")
		} else {
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)
			console.log(isPasswordCorrect);

			/*
				compareSync()
				will return a boolean value
				so if it matches, this will return true. 
				If not this will return false.
			*/

			if(isPasswordCorrect){
				return res.send({accessToken: auth.createAccessToken(foundUser)})
			} else {
				return res.send("Incorrect password, please try again")
			}
		}
	})
	.catch(error => res.send(error));

};


//-------SECTION: GETTING SINGLE USER DETAILS--------------------------

module.exports.getUserDetails = (req, res) =>{
	console.log(req.user);

	/* expected output:
		{	
			id: token
			email: test@email.com
			isAdmin: false
		}
	*/
	// find the logged in user's document from our Database and send it to the Client by it's id.

	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

//-------SECTION: GETTING EMAIL --------------------------


module.exports.checkEmailExists = (req, res) => {
	console.log(req.body)


	User.findOne({email: req.body.email})
	.then(foundEmail => {
		if(foundEmail === null){
			return res.send("Email is available to register")
		} else {
			const isEmailExists = (req.body.email, foundEmail.email)
			console.log(isEmailExists);


			if(isEmailExists){
				return res.send(`Email <${foundEmail.email}> is already registered`)
			} 
		}
	})
	.catch(error => res.send(error));

};


//-------SECTION: UPDATING USER DETAILS --------------------------

module.exports.updateUserDetails = (req, res) =>{
	console.log(req.body); //input for new values
	console.log(req.user.id); //check the logged in user's id

	let updates = {
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		mobileNumber: req.body.mobileNumber
	}


	User.findByIdAndUpdate(req.user.id, updates, {new: true})
	.then(updatedUser => res.send(updatedUser))
	.catch(error => res.send(error))

};
//-------SECTION: UPDATE AN ADMIN --------------------------

module.exports.updateAdmin = (req, res) =>{
	console.log(req.user.id); //id of the logged in user
	console.log(req.params.id); //id of the user to be updated

	let updates = {
		isAdmin: true
	}


	User.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedUser => res.send(updatedUser))
	.catch(error => res.send(error))

};

//-------SECTION: UPDATE AN ADMIN --------------------------

module.exports.enroll = async (req, res) =>{
	console.log(req.user.id); //id of the logged in user (with token)
	console.log(req.body.courseId) // the course from our request body.
	
	if(req.user.isAdmin){
		return res.send("Action Forbidden")
	}

	let isUserupdated = await User.findById(req.user.id).then(user =>{
		console.log(user);

		let newEnrollment = {
			courseId: req.body.courseId
		}

		user.enrollments.push(newEnrollment)
		return user.save()
		.then(user => true)
		.catch(error => error.message)
	})

		if(isUserupdated !== true){
		return res.send({message: isUserupdated})
		}

		let isCourseUpdated = await Course.findById(req.body.courseId).then (course =>{
			let enrollee = {
				userId: req.user.id
			}
			course.enrollees.push(enrollee)

			return course.save()
			.then(course => true)
			.catch(error => error.message)
		})

		if(isCourseUpdated !== true){
			return res.send({message: isCourseUpdated})
		}

		if(isUserupdated && isCourseUpdated){
			return res.send({message: 'User enrolled successfully'})
		}

};


//-----------------GET ENROLLMENTS -------------------------------------------

module.exports.getEnrollments = (req, res) =>{
	User.findById(req.user.id)
	.then(result => res.send(result.enrollments))
	.catch(error => res.send(error))
};


