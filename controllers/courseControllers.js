// import the user model in our controllers. So that our controllers or controller function may have access to our user model.
const Course = require("../models/Course");

const bcrypt = require('bcryptjs')



//----------------------Create Course---------------------------

module.exports.registerCourse = (req, res) => {
	console.log(req.body);

	let newCourse = new Course({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	});

	newCourse.save()
	.then(course => res.send(course))
	.catch(error => res.send(error));
};

//--------------------RETRIEVAL OF ALL Courses-----------------
module.exports.getAllCourses = (req, res) => {
	Course.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));
}

//-------SECTION: GETTING SINGLE COURSE--------------------------

module.exports.getSingleCourse = (req, res) =>{
	console.log(req.params);

	Course.findById(req.params.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

//--------------SECTION: Updating a Course--------------------------------
module.exports.updateCourse = (req, res) =>{
	let updates = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}
	Course.findByIdAndUpdate(req.params.id, updates, {new:true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(error => res.send(error))
}



//--------------SECTION: archiving a Course--------------------------------
module.exports.archiveCourse = (req, res) =>{
	console.log(req.user.id); //id of the logged in user
	console.log(req.params.id); //id of the course to be updated

	let updates = {
		isActive: false
	}


	Course.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(error => res.send(error))

};

//--------------SECTION: activate a Course--------------------------------
module.exports.activateCourse = (req, res) =>{
	console.log(req.user.id); //id of the logged in user
	console.log(req.params.id); //id of the course to be updated

	let updates = {
		isActive: true
	}


	Course.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(error => res.send(error))

};


//--------------SECTION: Get Active Courses--------------------------------


module.exports.getActiveCourses = (req, res) => {

	console.log(`showing active courses`)


	Course.find({isActive: true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(error => res.send(error))

};

//-----------------FIND COURSES BY NAME -------------------------------------------

module.exports.findCoursesByName = (req, res) =>{
	console.log(req.body) //contain the name of the course you are looking for.
	Course.find({name: {$regex: req.body.name, $options: '$i'}})
	.then(result => {
		if(result.length === 0){
			return res.send('No courses found')
		} else {
			return res.send(result)
		}
	})
	
};