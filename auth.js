// Reference: https://jwt.io

const jwt = require('jsonwebtoken');
const secret = "CourseBookingAPI"; //note this is one time only! should not be changed. unless all users of the site will be informed. update.

module.exports.createAccessToken = (user) => {
	console.log(user);


	//data object is created to declare some details of User
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
	console.log(data);
	return jwt.sign(data, secret, {});


}
/*
	1. You can only get access token when a user logs in in your app with the correct credentials.
	2. As a user, you can only get your own details from your own token from logging in.
	3. JWT is not meant for sensitive data.
	4. JWT is like a passport, you can use around the app to access certain features meant for your type of user.


*/


// goal: to check if the token exist 

module.exports.verify = (req, res, next) => {
	//req.headers.authorization contains jwt/ token
	let token = req.headers.authorization;

	if(typeof token === "undefined"){
		return res.send({auth: "Failed. No Token"})
	} else {
		
			//slice method is used for arrays and strings
			//bearer eydfoigfg909808909i30jfi
		
		token = token.slice(7, token.length)
		console.log(token)

		jwt.verify(token, secret, (error, decodedToken) =>{
			if(error){
				return res.send({
					auth: "Failed",
					message: error.message
				})
			} else {
				req.user = decodedToken
				next();
			}
		})
	}
};


// goal: to check if the token is ADMIN

module.exports.verifyAdmin = (req, res, next) => {

	if(req.user.isAdmin){
		next();
	} else {
		return res.send({
			auth: "Failed",
			message: "Action Forbidden"
		})
	}
};